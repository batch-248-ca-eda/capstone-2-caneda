const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type: String,
		required: [true, "User ID is required"]
	},
	totalAmount : {
		type: Number,
		default: 0
	},
	quantity: {
		type: Number,
		default: 0
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},
	products : [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			prodQuantity: {
				type: Number,
				default: 0
			},
			subTotal: {
				type: Number,
				defaul: 0
			},
			imgLink: {
				type: String
			},
			productName: {
				type: String
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);