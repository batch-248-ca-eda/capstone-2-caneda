const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId : {
		type: String,
		required: [true, "User ID is required"]
	},
	quantity: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		default: 0
	},
	productsInCart: [
		{
			productId: {
				type: String/*,
				required: [true, "Product ID required"]*/
				
			},
			prodQuantity: {
				type: Number/*,
				required: [true, "Product Quantity required"]*/
			},
			subTotal: {
				type: Number,
				default: 0
			},
			imgLink: {
				type: String/*,
				required: [true, "Product Image required"]*/
			},
			productName: {
				type: String/*,
				required: [true, "Product Name is  required"]*/
			}

		}
	]


})

module.exports = mongoose.model("Cart", cartSchema);