
const cors = require("cors");
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cartRoutes = require("./routes/cartRoutes");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");
const productRoutes = require("./routes/productRoutes");


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.5uy7vj2.mongodb.net/capstone-2-db?retryWrites=true&w=majority",
	
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);



let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"));

app.use("/carts", cartRoutes);
app.use("/users", userRoutes);
app.use("/orders", orderRoutes);
app.use("/products", productRoutes);



app.listen(process.env.PORT || 5000, ()=>{

	console.log(`API is now online on port ${process.env.PORT || 5000}`)
})
