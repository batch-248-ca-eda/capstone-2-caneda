const auth = require("../auth");
const bcrypt = require("bcrypt");
const User = require("../models/User");
const Order = require("../models/Order")



module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user,error)=>{

		if (error){
			return false
		} else {
			return true;
		}
	});
};


module.exports.userAuthentication = (reqBody) =>{

	return User.findOne({email: reqBody.email}).then(result=>{

		if(result == null){
			return false;
		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	});
};	


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result=>{

		if(result.length>0){
			return true;
		}else{
			return false;
		}
	})
}


module.exports.setUserAsAdmin = (reqParams, data, reqBody) => {

	// console.log(reqBody.userId)

	if (data.isAdmin){

		let userDat = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(reqParams._id, userDat).then((user,error)=>{

			if (error){
				return false
			} else {
				// user.isAdmin = userDat.isAdmin
				return true
			};
		});
	};
};


module.exports.setUserAsNonAdmin = (reqParams, data) => {

	console.log(data.isAdmin)

	if (data.isAdmin){

		let userDat = {
			isAdmin: false
		}

		return User.findByIdAndUpdate(reqParams._id, userDat).then((user,error)=>{

			if (error){
				return false
			} else {
				// user.isAdmin = userDat.isAdmin
				return true;
			};
		});

	} else {

		return false
	}
};



module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result=>{

		result.password = "";
		return result;
	})
}


module.exports.userOrders = (reqParams) => {

	return User.findById({_id: reqParams._id}).then(foundUser=>{
		
		console.log(foundUser)
		if (foundUser == null || foundUser == undefined){

			return `User is not registered`
			
		} else {

			return Order.find({userId: reqParams._id}).then((user,error)=>{

				if (user.length>0){
					return user;
				} else {
					false
				}

			})

		}
	})
}


module.exports.getAllUsers = () => {

	return User.find({}).then(result=>{
		console.log(result)
		return result;
	});
};





module.exports.deleteUser = (reqBody) => {

	return User.findByIdAndRemove(reqBody.userId);

	return true
}


