const Product = require("../models/Product");



module.exports.addProduct = (data) =>{

	if (data.isAdmin){

		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			imgLink: data.product.imgLink
		});
	
		return newProduct.save().then((product,error)=>{

			if (error){
				return false
			} else {
				return true;
			}
		})

	} else {
		return false
	}
};


module.exports.getAllActiveProd = () => {

	return Product.find({isActive: true}).then(result=>{
		return result;
	});
};


module.exports.getAllProducts = () => {

	return Product.find({}).then(result=>{
		return result;
	});
};


module.exports.getSingleProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result=>{
		return result;
	})
}


module.exports.updateProdInfo = (reqParams, reqBody)=>{

		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			imgLink: reqBody.imgLink
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update,error)=>{

				if (error){
					return false
				} else {
					return true
				}
		});
	};



module.exports.archiveProd = (reqParams, data) => {

	if (data.isAdmin){

		let archivedProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(reqParams._id, archivedProduct).then((archived,error)=>{

			if (error){
				return false
			} else {
				return true
			}
		});
	};
};


module.exports.activateProd = (reqParams, data) => {

	if (data.isAdmin){

		let activatedProduct = {
			isActive: true
		}

		return Product.findByIdAndUpdate(reqParams._id, activatedProduct).then((activated,error)=>{

			if (error){
				return false
			} else {
				return true
			}
		});
	};
};


module.exports.deleteProd = (reqBody) => {
	console.log(reqBody.productId)
	return Product.findByIdAndRemove(reqBody.productId);

	return true
}