const auth = require("../auth");
const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");



module.exports.createOrd = (reqBody) => {

	return Cart.findOne({userId: reqBody.userId}).then(foundCart => {
		console.log(foundCart.id)
		// console.log(foundCart.userId)
		// console.log(foundCart.total)
		// console.log(foundCart.productsInCart)

		let newOrder = new Order ({
			userId: foundCart.userId,
			totalAmount: foundCart.total,
			quantity: foundCart.quantity,
			products: foundCart.productsInCart
		})

		return newOrder.save().then((ord,error) => {
			if (error) {

				return false 
				
			} else {

				return Cart.findByIdAndRemove(foundCart.id);

				return true
			}
		})

	})
};


module.exports.getAllOrders = (data) => {

	console.log(data.isAdmin)
	if (data.isAdmin){

		return Order.find({}).then(result=>{

			return result;
		});	

	} else {
		false
	}
};



module.exports.getOrder = (reqParams) => {
	// console.log(reqParams.userId)

	return Order.findOne({userId: reqParams.userId}).then(result=>{
		console.log(result)
		return result
	});
};