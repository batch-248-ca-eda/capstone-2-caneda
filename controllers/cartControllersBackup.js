const ObjectId = require("mongodb")
const User = require("../models/User");
const Cart = require("../models/Cart");
const Product = require("../models/Product");
const util = require('util')



module.exports.addToCart = async (reqParams,data,reqBody) => {

	
	
	console.log(data.cart)
	return findingUser = await User.find({_id: reqParams._id}).then(result=>{

		if (result[0] == undefined || result[0] == null){
			return `User not found!`

		} else {

			return findingCart = Cart.find({userId: reqParams._id}).then(cartFound=>{

				if (cartFound[0] == undefined || cartFound[0] == null){
					
					return finddingProdPrice = Product.findById({_id: data.cart.productsInCart[0].productId}).then(foundPrice=>{
						console.log(foundPrice)
						let newCart = new Cart({
							userId: reqParams._id,
							total: foundPrice.price * data.cart.productsInCart[0].prodQuantity,
							quantity: data.cart.productsInCart[0].prodQuantity,
							productsInCart: [
								{
									productId: data.cart.productsInCart[0].productId,
									prodQuantity: data.cart.productsInCart[0].prodQuantity,
									subTotal: foundPrice.price * data.cart.productsInCart[0].prodQuantity
								}
							]
						})	

						newCart.save()
						return `You added ${data.cart.productsInCart[0].prodQuantity} ${foundPrice.name} with a subtotal of P${foundPrice.price * data.cart.productsInCart[0].prodQuantity}`

					})

				} else {

					return findProduct = Product.findById({_id: data.cart.productsInCart[0].productId}).then(foundProd=>{

						if (foundProd == undefined || foundProd == null){
							
							return `Product does not exist`

						} else {

							return findQuan = Cart.findById(cartFound[0].id).then(foundQuan=>{

									let subto = data.cart.productsInCart[0].prodQuantity * foundProd.price
									let updateQuan = {
										quantity: foundQuan.quantity + data.cart.productsInCart[0].prodQuantity,
										total: foundQuan.total + subto
									}
									
								     return cart2update = Cart.findByIdAndUpdate(cartFound[0].id,updateQuan).then(result=>{
									
										result.productsInCart.push({
											productId: data.cart.productsInCart[0].productId,
											prodQuantity: data.cart.productsInCart[0].prodQuantity,
											subTotal: data.cart.productsInCart[0].prodQuantity * foundProd.price
										})

									result.save()
									console.log(foundProd.name)
									return `You added ${data.cart.productsInCart[0].prodQuantity} ${foundProd.name} with a subtotal of P${subto}`
								})
							})
						}
					})
				}
			})
		}
	})
}



module.exports.changeQuan2 = async (reqParams,data) => {

	const findingPrice = await Product.find({_id: data.productId}).then(foundPrice2=>{
		console.log(foundPrice2)
	let subto = data.prodQuantity * foundPrice2[0].price;
	const findingCart = Cart.find({id: reqParams.id}).then(foundCart=>{
	
	try {
		const result = Cart.findOneAndUpdate(
			{'productsInCart._id': reqParams.id},
			{$set: {'productsInCart.$.productId': data.productId,
					'productsInCart.$.prodQuantity': data.prodQuantity,
					'productsInCart.$._id': reqParams.id,
					'productsInCart.$.subTotal': subto} },
			{new: true},console.log.productsInCart
			);

		if (result){
			console.log(`quantity updated!`)
			return result
		} else {
			return status(404).json({error: 'something went wrong1'})
		}
	} catch (e) {
		console.log(e.message);
		return status(500).json({error: 'something went wrong2'});
	}
	})
	})
}



module.exports.deleteFromCarts = async (reqParams,reqBody) =>{
	console.log(reqParams.id);
	console.log(reqBody._id)
	try {
		const result = Cart.updateOne({"_id": reqParams.id},
									{
										"$pull":{
											"productsInCart":{
												"_id": reqBody._id
											}
										}
									});

		if (result){
			//console.log(result)
			return result
		} else {
			false
		}

	} catch  (e) {
		console.log(e.message);
		return `status(500).json({error: 'something went wrong2'});`
	}

}

module.exports.findingIt = (data) => {

	return Cart.find({"productsInCart.findingId": 'abc123'}).then(result=>{
		console.log(data._id);
		console.log(result[0].productsInCart);
		result[0].subTotal = 5000;
		return result;
	})
}


module.exports.total = (reqParams,reqBody)=>{

	try {
		const result = Cart.findOneAndUpdate(
			{'_id': reqParams.id},
			{$set: {'quantity': reqBody.quantity,
					'total': reqBody.total} },
			{new: true}
			);

		if (result){
			console.log(`Cart udpated!!`)
			return result
		} else {
			return status(404).json({error: 'something went wrong1'})
		}
	} catch (e) {
		console.log(e.message);
		return status(500).json({error: 'something went wrong2'});
	}

}
