const ObjectId = require("mongodb")
const User = require("../models/User");
const Cart = require("../models/Cart");
const Product = require("../models/Product");
const util = require('util')



module.exports.addToCart = async (reqParams,reqBody) => {

	
	
	// console.log(reqBody.userId)
	// console.log(reqParams.productId)
	return findingUser = await User.find({_id: reqBody.userId}).then(result=>{

		if (result[0] == undefined || result[0] == null){
			return `User not found!`

		} else {

			return findingCart = Cart.find({userId: reqBody.userId}).then(cartFound=>{

				if (cartFound[0] == undefined || cartFound[0] == null){
					 
					return finddingProdPrice = Product.findById({_id: reqParams.productId}).then(foundPrice=>{
						console.log(foundPrice)
						let newCart = new Cart({
							userId: reqBody.userId,
							total: foundPrice.price * reqBody.quantity,
							quantity: reqBody.quantity,
							productsInCart: [
								{
									productId: reqParams.productId,
									prodQuantity: reqBody.quantity,
									subTotal: foundPrice.price * reqBody.quantity,
									imgLink: reqBody.imgLink,
									productName: reqBody.productName
								}
							]
						})	

						newCart.save()
						return true

					})

				} else {

					return findProduct = Product.findById({_id: reqParams.productId}).then(foundProd=>{

						if (foundProd == undefined || foundProd == null){
							
							return `Product does not exist`

						} else {

							return findQuan = Cart.findById(cartFound[0].id).then(foundQuan=>{

									let subto = reqBody.quantity * foundProd.price
									let updateQuan = {
										quantity: foundQuan.quantity + reqBody.quantity,
										total: foundQuan.total + subto
									}
									
								     return cart2update = Cart.findByIdAndUpdate(cartFound[0].id,updateQuan).then(result=>{
									
										result.productsInCart.push({
											productId: reqParams.productId,
											prodQuantity: reqBody.quantity,
											subTotal: reqBody.quantity * foundProd.price,
											imgLink: reqBody.imgLink,
											productName: reqBody.productName
										})

									result.save()
									console.log(reqBody.imgLink)
									return true
								})
							})
						}
					})
				}
			})
		}
	})
}





module.exports.changeQuan2 = async (reqParams,data) => {

	const findingPrice = await Product.find({_id: data.productId}).then(foundPrice2=>{
		console.log(foundPrice2)
	let subto = data.prodQuantity * foundPrice2[0].price;
	const findingCart = Cart.find({id: reqParams.id}).then(foundCart=>{
	
	try {
		const result = Cart.findOneAndUpdate(
			{'productsInCart._id': reqParams.id},
			{$set: {'productsInCart.$.productId': data.productId,
					'productsInCart.$.prodQuantity': data.prodQuantity,
					'productsInCart.$._id': reqParams.id,
					'productsInCart.$.subTotal': subto} },
			{new: true},console.log.productsInCart
			);

		if (result){
			console.log(`quantity updated!`)
			return result
		} else {
			return status(404).json({error: 'something went wrong1'})
		}
	} catch (e) {
		console.log(e.message);
		return status(500).json({error: 'something went wrong2'});
	}
	})
	})
}


module.exports.deductTotal = (reqBody) =>{
	// console.log(reqBody.prodQuantity)
	console.log(`asd`)
	return findCart = Cart.findOne({userId: reqBody.userId}).then(foundTotal => {
		//console.log(foundTotal.quantity)
		let totaled = foundTotal.total - reqBody.amountToDeduct
		let prodQuan = foundTotal.quantity - reqBody.prodQuantity
		
		let update = {
			total: totaled,
			quantity: prodQuan 
		}
		
		// console.log(update)
			return findCartAgain = Cart.findByIdAndUpdate(foundTotal.id, update). then((updated,error) => {
				// console.log(updated)
				if (error){
					return false
				} else {
					return updated
				}
			})
	})

}



module.exports.deleteFromCarts = async (reqBody) =>{
	// console.log(reqBody.cartId);
	console.log(reqBody.user)
	// console.log(reqBody.cart._id)
	// console.log(reqBody.reqBody.cart._id)
	try {
		const result = Cart.updateOne({"userId": reqBody.userId},
									{
										"$pull":{
											"productsInCart":{
												"_id": reqBody.arrayId
											}
										}
									});

		if (result){
			
			return findCart = Cart.findOne({userId: reqBody.userId}).then(foundTotal => {
				console.log(foundTotal.id)
				let totaled = foundTotal.total - reqBody.amountToDeduct
				let prodQuan = foundTotal.quantity - reqBody.prodQuantity

				let update = {
					total: totaled,
					quantity: prodQuan 
				}
				
				// console.log(update)
					return findCartAgain = Cart.findByIdAndUpdate(foundTotal.id, update). then((updated,error) => {
						console.log(updated)
						if (error){
							return false
						} else {
							return result
						}
					})
			})

			
		} else {
			false
		}

	} catch  (e) {
		console.log(e.message);
		return `status(500).json({error: 'something went wrong2'});`
	}

}

module.exports.findingIt = (data) => {

	return Cart.find({"productsInCart.findingId": 'abc123'}).then(result=>{
		console.log(data._id);
		console.log(result[0].productsInCart);
		result[0].subTotal = 5000;
		return result;
	})
}


module.exports.total = (reqParams,reqBody)=>{

	try {
		const result = Cart.findOneAndUpdate(
			{'_id': reqParams.id},
			{$set: {'quantity': reqBody.quantity,
					'total': reqBody.total} },
			{new: true}
			);

		if (result){
			console.log(`Cart udpated!!`)
			return result
		} else {
			return status(404).json({error: 'something went wrong1'})
		}
	} catch (e) {
		console.log(e.message);
		return status(500).json({error: 'something went wrong2'});
	}

}



module.exports.viewUserCart = (reqParams)=> {
	// console.log(reqParams.userId)
	return Cart.find({userId: reqParams.userId}).then(carty=>{
		// console.log(carty.id)
		// console.log(carty)
		return carty
	})
}


module.exports.viewCart = (reqParams, reqBody)=>{

	return Cart.findOne({userId: reqParams.userId}).then(foundCart=>{
		
		// if (foundCart.productsInCart.length === 0) { 
		// 	 return false
		// } else {
			// let res = foundCart.productsInCart

				// Object.values(foundCart).map(x=>console.log())

			if (foundCart === null || foundCart === undefined || foundCart.quantity === 0) {
				
				return false

			} else {

				return foundCart.productsInCart
			} 
		//}
	})
}


