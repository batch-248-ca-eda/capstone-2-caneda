const auth = require("../auth")
const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");



router.post("/createOrder",(req,res)=>{ 

	orderController.createOrd(req.body).then(resultFromOrderController=>res.send(resultFromOrderController));
});

router.get("/all", auth.verify,(req,res)=>{

	const authen = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.getAllOrders(authen).then(resultFromOrderController=>res.send(resultFromOrderController));
})


router.get("/getOrder/:userId",(req,res)=>{

	orderController.getOrder(req.params).then(resultFromOrderController=>res.send(resultFromOrderController));
})


module.exports = router;




/*
router.post("/createOrder",(req,res)=>{ 

	let orderData = {
		totalAmount: req.body,
		order: req.body,
		products: [
			{
				productId: req.body,
				quantity: req.body
			}
		]
	}

	orderController.createOrd(orderData).then(resultFromOrderController=>res.send(resultFromOrderController));
});
*/



/*
module.exports.createOrd = async (data) => {

	let findUser = await User.findById(data.userId).then(user=>{

		let newOrder = new Order({
			userId: data.order.userId,
			products: data.order.products 
		})

		console.log(user)

		return newOrder.save().then((ord,error)=>{

			if(error){

				return false

			} else {

				console.log(ord.id)//for console verification

				let updateProd = Product.findById(data.order.products[0].productId).then(prod=>{

					console.log(prod)
					prod.orders.push({
						orderId: ord.id,
						quantity: data.order.products[0].quantity
					})
					prod.save()
					//console.log(prod)
					return Order.findByIdAndUpdate(ord.id).then(orde=>{

						 orde.totalAmount = prod.price * data.order.products[0].quantity;

						 return orde.save()
						
					})

					return prod.save()
				})

				return true
			};
		});
	})

	if (findUser){

		//console.log(data.order.userId);//for console verification
		return Product.findById(data.order.products[0].productId).then(resProdName=>{

			return User.findById(data.order.userId).then(resUserName=>{

				total = resProdName.price*data.order.products[0].quantity;
					
				return `You ordered ${data.order.products[0].quantity} (${resProdName.name}) with a total of P${total} 
					using this account (Name: ${resUserName.firstName} User ID: ${data.order.userId}) `
			});
		});

	} else {

		return false
	};

};
*/
