const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userControllers");



router.post("/register",(req,res)=>{
	
	userController.registerUser(req.body).then(resultFromUserController=>res.send(resultFromUserController));
})


router.post("/login",(req,res)=>{

	userController.userAuthentication(req.body).then(resultFromUserController=>res.send(resultFromUserController));
})


router.post("/checkEmailExists",(req,res)=>{

	userController.checkEmailExists(req.body).then(resultFromUserController=>res.send(resultFromUserController));
})


router.put("/updateToAdmin/:_id", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	userController.setUserAsAdmin(req.params, data).then(resultFromUserController=>res.send(resultFromUserController));
})

router.put("/updateToNonAdmin/:_id", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	userController.setUserAsNonAdmin(req.params, data).then(resultFromUserController=>res.send(resultFromUserController));
})


router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));

})


router.get("/orders/:_id", (req,res)=>{

	userController.userOrders(req.params).then(resultFromUserController=>res.send(resultFromUserController));
})


router.get("/all", auth.verify, (req,res)=>{

	userController.getAllUsers().then(resultFromUserController=>res.send(resultFromUserController));
})


router.delete("/delete", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	userController.deleteUser(req.body).then(resultFromUserController=>res.send(resultFromUserController));
})


module.exports = router;