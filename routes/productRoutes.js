const auth = require("../auth")
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");



router.post("/", auth.verify,(req,res)=>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromProductController=>res.send(resultFromProductController));
})


router.get("/",(req,res)=>{

	productController.getAllActiveProd().then(resultFromProductController=>res.send(resultFromProductController));
})


router.get("/all",(req,res)=>{

	productController.getAllProducts().then(resultFromProductController=>res.send(resultFromProductController));
})


router.get("/:productId",(req,res)=>{

	productController.getSingleProduct(req.params).then(resultFromProductController=>res.send(resultFromProductController));
})


router.put("/updateProduct/:productId", auth.verify, (req,res)=>{

	productController.updateProdInfo(req.params, req.body).then(resultFromProductController=>res.send(resultFromProductController));
})


router.put("/archiveProduct/:_id", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	productController.archiveProd(req.params, data).then(resultFromProductController=>res.send(resultFromProductController));
})


router.put("/activateProduct/:_id", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	productController.activateProd(req.params, data).then(resultFromProductController=>res.send(resultFromProductController));
})

router.delete("/delete", auth.verify, (req,res)=>{

	const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

	productController.deleteProd(req.body).then(resultFromProductController=>res.send(resultFromProductController));
})


module.exports = router;