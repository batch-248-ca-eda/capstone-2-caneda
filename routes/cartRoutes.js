const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartControllers");
const Cart = require("../models/Cart");



router.post("/:productId",(req,res)=>{

	cartController.addToCart(req.params, req.body).then(resultFromCartController=>res.send(resultFromCartController));
});


router.patch("/:id/changeQuantity", (req,res)=>{

	cartController.changeQuan2(req.params,req.body).then(resultFromCartController=>res.send(resultFromCartController));
})


router.delete("/deleteFromCart", (req,res)=>{

	cartController.deleteFromCarts(req.body).then(resultFromCartController=>res.send(resultFromCartController));
})

router.get("/viewUserCart/:userId",(req,res)=>{

	cartController.viewUserCart(req.params).then(resultFromCartController=>res.send(resultFromCartController));
})


router.get("/:userId",(req,res)=>{

	cartController.viewCart(req.params, req.body).then(resultFromCartController=>res.send(resultFromCartController));
})


/*
router.patch("/:id/changeQuantity", async(req,res)=>{
	console.log(req.params);
	const cartId = req.params.id;
	try {
		const result = await Cart.findOneAndUpdate(
			{'productsInCart._id': cartId},
			{$set: {'productsInCart.$': req.body}},
			{new: true}
		);

		console.log(result);

		if (result){
			res.json(result)
		} else {
			res.status(404).json({error: 'something went wrong1'})
		}
	} catch (e) {
		console.log(e.message);
		res.status(500).json({error: 'something went wrong2'});
	}
});
*/

router.post("/finding/:_id",(req,res)=>{

	cartController.findingIt(req.params).then(resultFromCartController=>res.send(resultFromCartController));
})


/*
router.patch("/:id/changeQuantity", async(req,res)=>{
	console.log(req.params);
	const cartId = req.params.id;
	try {
		await Cart.findOneAndUpdate(
			{'productsInCart._id': cartId},
			{$set: {'productsInCart.$': req.body}},
			{new: true}
		);
	} catch (e) {
		console.log(e.message);
		res.status(500).json({error: 'something went wrong'});
	}
});

*/


router.patch("/totalPrice/:id",(req,res)=>{

	cartController.total(req.params,req.body).then(resultFromCartController=>res.send(resultFromCartController));
})


router.put("/deductTotal",(req,res) => {

	cartController.deductTotal(req.body).then(resultFromCartController=>res.send(resultFromCartController));
})






module.exports = router;